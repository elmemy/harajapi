<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::post('logout', 'Auth\LoginController@logout');
Route::post('updatePassword', 'UserController@updatePassword');
Route::post('facebook', 'UserController@facebook');
Route::post('twitter', 'UserController@twitter');
//categories
Route::post('addCategory', 'CategoryController@addCategory');
Route::post('addsubCategory', 'CategoryController@addsubCategory');
//Ads
Route::post('addAds', 'AdsController@addAds');
Route::get('getAds', 'AdsController@getAds');
//Comments
Route::post('addComment', 'CommentsController@addComment');
//follow
Route::post('followUser', 'FollowerController@followUser');

//user with follow and followrs and Ads
Route::get('user/{token}', 'UserController@getAuthUser');
Route::get('users/{id}', 'UserController@getUser');
//AllCategory
Route::get('allcategory', 'CategoryController@AllCategory');
Route::get('subcategory/{id}', 'CategoryController@subcategory');
//Message
Route::post('sentmessage','MessageController@sentmessage');
Route::get('sentmessage/{token}','MessageController@ShowMessages');
//like
Route::post('makelike','LikeController@makelike');
//favourite
Route::post('makefav','FavouriteController@makefav');
Route::post('likeAds','AdsController@likeAds');
//search
Route::get("usersearch","UserController@UserSearch");
Route::post("adssearch","AdsController@AdsSearch");
//Adv Search
Route::post("/advsearch","SearchController@advsearch");
Route::get("/searchcar","SearchController@searchCar_index");
Route::get("/searchtypecar/{parent_id}","SearchController@searchtype_Car");
Route::post("/searchcar","SearchController@searchCar");
//Address
Route::post("addadrs","AddressController@addAdrs");
//get category and Ads
Route::get("/getcatpro/{id}","SearchController@getCategoryProducts");
