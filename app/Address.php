<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
  protected $table = 'address';

  protected $fillable = [
    'name', 'parent_id',
  ];

  public function ads()
  {
    return $this->hasMany('App\Ads','parent_id');
  }
}
