<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
  protected $table = 'category';

  protected $fillable = [
    'name', 'image', 'parent_id',
  ];

  public function ads()
    {
        return $this->hasMany('App\Ads');
    }
}
