<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
  protected $table = 'like';

  protected $fillable = [
    'user_id', 'Ads_id',
  ];


  public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

  public function ads()
  {
    return $this->belongsTo(Ads::class, 'Ads_id', 'id');
  }
}
