<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Favourite;
use App\User;

class FavouriteController extends Controller
{
  public function makefav(Request $request)
  {
    if (!$request->token)
        return response()->json(['msg' => 'token_Required']);
    if (!$request->Ads_id)
        return response()->json(['msg'=>'Ads_id_Required']);
    $user = User::where('tokens', $request->token)->first();
        if (!$user)
          return response()->json(['msg' => 'invalid_Token']);
        $fav = new Favourite;
        $fav->user_id = $user->id;
        $fav->Ads_id = $request->Ads_id;
        $fav->save();
        return response()->json(['msg' => 'Favourite created successfully']);
  }

}
