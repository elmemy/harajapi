<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Message;
use Auth;
class MessageController extends Controller
{
   public function sentmessage(Request $request)
    {
        if (!$request->token)
         return response()->json(['msg' => 'token_Required']);
        if (!$request->to_user)
          return response()->json(['msg'=>'to_user_Required']);
        if (!$request->content)
          return response()->json(['msg'=>'content_Required']);

      $user = User::where('tokens', $request->token)->first();
            if (!$user)
              return response()->json(['msg' => 'invalid_Token']);
      $to_user= User::where('id', $request->to_user)->first();
                  $msg = new Message;
                  $msg->to_user = $request->to_user;
                  $msg->from_user = $user->id;
                  $msg->content = $request->content;
                  $msg->save();
                  return response()->json(['msg' => 'message send successfully']);
    }


 public function ShowMessages($token)
  {
      $all = User::where('tokens', $token)->with('Message')->get();
          return response()->json(['result' => $all]);

  }
}
