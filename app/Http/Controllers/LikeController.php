<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Like;

class LikeController extends Controller
{
    public function makelike(Request $request)
    {
      if (!$request->token)
          return response()->json(['msg' => 'token_Required']);
      if (!$request->Ads_id)
          return response()->json(['msg'=>'Ads_id_Required']);
      $user = User::where('tokens', $request->token)->first();
          if (!$user)
            return response()->json(['msg' => 'invalid_Token']);
          $like = new Like;
          $like->user_id = $user->id;
          $like->Ads_id = $request->Ads_id;
          $like->save();
          return response()->json(['msg' => 'like created successfully']);
    }


}
