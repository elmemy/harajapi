<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ads;
use App\category;

class SearchController extends Controller
{
  public function advsearch(Request $request)
  {
    $ser = Ads::where('title', 'LIKE', '%'.$request->text.'%');
    if($request->parent){
      $ser->where('parent_id', $request->parent);
    }
    if($request->address){
      $ser->where('address', $request->address);
    }
    $result = $ser->get();
    return response()->json(['result' => $result]);
  }

  public function searchCar_index()
  {
    $car = category::where('parent_id' , 3)->get();

    return response()->json(['cars' => $car]);
  }
  public function searchtype_Car($parent_id)
  {
    $type_car = category::where('id' , $parent_id)->get();

    return response()->json(['typecars' => $type_car]);
  }
  public function searchCar(Request $request)
  {
    $sercar = Ads::where('title', 'LIKE', '%'.$request->text.'%');
    if($request->parent){ //النوع
      $sercar->where('parent_id', $request->parent);
    }
    if($request->year){   //الموديل
      $sercar->where('year', $request->year);
    }
    $result = $sercar->get();
    return response()->json(['result' => $result]);
  }

}
