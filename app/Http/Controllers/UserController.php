<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Follower;
use App\Ads;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{
    private $user;

  public function __construct(User $user)
      {
        $this->user = $user;
      }
//receive data from login
  private function receiveData($user_id, $token)
    {
      if (!$user_id)
          return response()->json(['msg' => 'user_id_Required']);
      if (!$token)
          return response()->json(['msg' => 'token_Required']);
        $user = User::where('id', $user_id)->first();
      if (!$user)
          return response()->json(['msg' => 'invalid_user_id']);
      }

//login
  public function login(Request $request)
      {
          if (!$request->token)
              return response()->json(['msg' => 'token_Required']);
          if (!$request->login)
              return response()->json(['msg' => 'login_Required']);
          if (!$request->password)
              return response()->json(['msg' => 'password_Required']);

          $field = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL) ? 'email' : 'name';

          $request->merge([$field => $request->input('login')]);
          $user = User::where($field, \request($field))->first();

          if (Auth::attempt($request->only($field, 'password'))) {
              $this->receiveData(Auth::user()->id, $request->token);
              if (Auth::user()->tokens)
                  return response()->json(['token' => Auth::user()->tokens]);
              else {
                  Auth::user()->tokens = str_random(150);
                  Auth::user()->save();
                  return response()->json(['token' => Auth::user()->tokens]);
              }
          }
          return response()->json(['msg' => 'invalid_data']);
      }


//Register
      public function register(Request $request)
      {
          if (!$request->name)
              return response()->json(['msg' => 'name_Required']);
          if (!$request->email)
              return response()->json(['msg' => 'email_Required']);
          if (!$request->password)
              return response()->json(['msg' => 'password_Required']);
          if (!$request->phone)
              return response()->json(['msg' => 'phone_Required']);

          $old_user = User::where('email', $request->email)->first();
          if ($old_user)
              return response()->json(['msg', 'email exist in the database']);
          if (!filter_var($request->email, FILTER_VALIDATE_EMAIL))
              return response()->json(['msg', 'invalid email']);
          $user = $this->user->create([
              'name' => $request->get('name'),
              'email' => $request->get('email'),
              'phone' => $request->get('phone'),
              'password' => bcrypt($request->get('password')),
              'tokens' => str_random(60)
          ]);
          return response()->json(['msg' => 'User created successfully']);
      }
//Update Password
      public function updatePassword(Request $request)
          {
              if (!$request->token)
                  return response()->json(['error' => 'token required']);
              if (!$request->password)
                  return response()->json(['error' => 'password required']);
              if (!$request->new_password)
                  return response()->json(['error' => 'new password required']);
              if (!$request->confirm_password)
                  return response()->json(['error' => 'confirm password required']);
              if ($request->new_password != $request->confirm_password)
                  return response()->json(['error' => 'password Not match']);
              $user = User::where('tokens', $request->token)->first();
              if ($user) {
                  if (!Hash::check($request->password, $user->password))
                      return response()->json(['error' => 'old password Not exist']);
              }
              $user->password = bcrypt($request->new_password);
              if ($user->save())
                  return response()->json(['success', 'password updated successfully']);
          }
//Facebook and Twitter

  public function facebook(Request $request)
    {
        if ($request->facebook_id) {
            $user = User::where('facebook', $request->facebook_id)->first();
            if (!$request->token)
                return response()->json(['msg' => 'token_Required']);

            if ($user) {
                $this->receiveData($user->id, $request->token);
                return response()->json(['token' => $user->tokens]);
            } else {
                $users = new User();
                $users->facebook = $request->facebook_id;
                $users->name = $request->fname . ' ' . $request->lname;
                $users->tokens = str_random(150);
                $users->save();
                $this->receiveData($user->id, $request->token);
                return response()->json(['token' => $users->tokens]);
            }
        } else
            return response()->json(['msg' => 'invalid data']);
    }
  public function twitter(Request $request)
      {
          $user = User::where('twitter', $request->twitter_id)->first();
          if (!$request->token)
              return response()->json(['msg' => 'token_Required']);

          if ($user) {
            $this->receiveData($user->id, $request->token);
              return response()->json(['token' => $user->tokens]);
          } else {
              $users = new User();
              $users->twitter = $request->twitter_id;
              $users->name = $request->name;
              $users->tokens = str_random(150);
              $users->save();
              $this->receiveData($users->id, $request->token);
              return response()->json(['token' => $users->tokens]);
          }
          return response()->json(['msg' => 'invalid data']);
      }



//get user ads ads with id
    public function getUser($id)
      {
        $user = User::where('id', $id)->with(['Ads' => function($query) {
        $query->orderBy('created_at');},'follower', 'following'])->paginate('10');
        if (!$user)
         return response()->json(['msg' => 'invalid_id']);
        foreach ($user as $u) {
          foreach ($u->follower as $f) {
            $query = Follower::where('user_id',$f->id)->where('follower_id',$u->id)->first();
            if ($query == null) {
                $f['follow'] = 'false';
            } elseif ($query != null) {
                $f['follow'] = 'true';
            }
          }
          foreach ($u->following as $f) {
            $query = Follower::where('user_id', $f->id)->where('follower_id', $u->id)->first();
            if ($query == null) {
                $f['follow'] = 'false';
            } elseif ($query != null) {
                $f['follow'] = 'true';
            }
          }
        }
          return response()->json(['result' => $user]);

      }

//get user with follow and Ads
public function getAuthUser($token)
  {

      $user = User::where('tokens', $token)->with(['Ads' => function($query) {
      $query->orderBy('created_at');},'follower', 'following'])->paginate('10');
      if (!$user)
          return response()->json(['msg' => 'invalid_Token']);
      foreach ($user as $u) {
          foreach ($u->follower as $f) {
              $query = Follower::where('user_id', $f->id)->where('follower_id', $u->id)->first();
              if ($query == null) {
                  $f['follow'] = 'false';
              } elseif ($query != null) {
                  $f['follow'] = 'true';
              }
          }
          foreach ($u->following as $f) {
              $query = Follower::where('user_id', $f->id)->where('follower_id', $u->id)->first();
              if ($query == null) {
                  $f['follow'] = 'false';
              } elseif ($query != null) {
                  $f['follow'] = 'true';
              }
          }
      }
      return response()->json(['result' => $user]);
  }
//UserSearch
public function UserSearch(Request $request)
    {
      if (!$request->search)
          return response()->json(['msg' => 'search_Required']);

    	if($request->has('search')){
    		$users = User::search($request->get('search'))->get();
    	}else{
    		$users = User::get();
    	}

      return response()->json(['result' => $users]);
    }

}
