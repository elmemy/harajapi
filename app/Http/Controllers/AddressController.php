<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
class AddressController extends Controller
{
    public function addAds(Request $request)
    {
      if (!$request->name)
          return response()->json(['msg' => 'name_Required']);
          
    $adr = new Address;
    $adr->name = $request->name;
    $adr->parent_id = $request->parent_id;
    $adr->save();
    return response()->json(['msg' => 'Address created successfully']);

    }
}
