<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comments;
use App\User;

class CommentsController extends Controller
{


public function addComment(Request $request)
  {
   if (!$request->token)
        return response()->json(['msg' => 'token_Required']);
    if (!$request->content)
        return response()->json(['msg' => 'content_Required']);
   if (!$request->ads_id)
        return response()->json(['msg' => 'post_id_Required']);


    $user = User::where('tokens', $request->token)->first();
      if (!$user)
        return response()->json(['msg' => 'invalid_Token']);
    $comment = new Comments;
    $comment->content = $request->content;
    $comment->user_id = $user->id;
    $comment->ads_id = $request->ads_id;
    $comment->save();


    return response()->json(['msg' => 'Comment created successfully']);
  }
}
