<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ads;
use App\User;
use App\Like;
use App\Favourite;
use App\category;
use Image;

class AdsController extends Controller
{

  public function addAds(Request $request)
    {
        if (!$request->title)
            return response()->json(['msg' => 'title_Required']);
        // if (!$request->address)
        //   return response()->json(['msg' => 'address_Required']);
        if (!$request->image)
          return response()->json(['msg' => 'image_Required']);
        if (!$request->phone)
          return response()->json(['msg' => 'phone_Required']);
      if (!$request->parent_id)
          return response()->json(['msg' => 'parent_id_Required']);

        $user = User::where('tokens', $request->token)->first();
          if (!$user)
            return response()->json(['msg' => 'invalid_Token']);


        $ads = new Ads;
        $ads->title = $request->title;
        $ads->address = $request->address;
        $ads->phone = $request->phone;
        $ads->year = $request->year;
        $ads->parent_id = $request->parent_id;
        $ads->user_id = $user->id;
        $image =  unique_file(request('image')->getClientOriginalName());
             $request->file('image')->move(
                 base_path() . '/public/images',$image);
      $ads->image =$image;
        $ads->save();

        return response()->json(['msg' => 'Ads created successfully']);
    }
    public function getAds()
    {
       // $ads = Ads::get()->pluck('user_id','title');
    $ads = Ads::orderBy('created_at', 'desc')
        ->with('User')
        ->withCount('Like', 'comment','fav')->paginate('10');

      foreach ($ads as $ad) {
        $isLiked = Like::where('Ads_id',$ad->id)->where('user_id',$ad->User->id)->first() ? true : false;
        $isFav = Favourite::where('Ads_id',$ad->id)->where('user_id',$ad->User->id)->first() ? true : false;
        $time = $ad->created_at->diffForHumans();
        $ad['since'] = $time;
        $ad['isLiked'] = $isLiked;
        $ad['isFav'] = $isFav;
      }
    return response()->json($ads);
    }

     public function likeAds(Request $request)
    {
        if (!$request->token)
            return response()->json(['msg' => 'token_Required']);
        if (!$request->Ads_id)
            return response()->json(['msg' => 'Ads_id_Required']);
        $user = User::where('tokens', $request->token)->first();
        if (!$user)
            return response()->json(['msg' => 'invalid_Token']);
        $likes = Like::where('user_id', $user->id)->where('Ads_id', $request->Ads_id)->first();
        if ($likes) {
            $like_count = Like::where('Ads_id', $request->Ads_id)->count();
            return response()->json(['unlike' => 0, 'count' => $like_count]);
        }
    }
    //AdsSearch
public function AdsSearch(Request $request)
   {
     if($request->has('search')){
       $ads = Ads::search($request->search)->get();
     }else{
       $ads = Ads::get();
     }

     return response()->json(['result' => $ads]);
   }
}
