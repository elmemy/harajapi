<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Follower;

class FollowerController extends Controller
{

public function followUser(Request $request)
  {

     if (!$request->token)
            return response()->json(['msg' => 'token_Required']);
    if (!$request->follower_id)
            return response()->json(['msg' => 'follower_id_Required']);

    $user = User::where('tokens', $request->token)->first();
    $follow_owner = User::where('id', $request->follower_id)->first();
      if(!$user)
        return response()->json(['msg' => 'invalid Token']);


        $fol = new Follower;
        $fol->user_id = $request->follower_id;
        $fol->follower_id = $user->id;
        $fol->save();


        return response()->json(['msg' => 'follow created successfully']);

  }

}
