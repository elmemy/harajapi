<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;

class CategoryController extends Controller
{
  public function addCategory(Request $request)
    {

      if (!$request->name)
          return response()->json(['msg' => 'name_Required']);

      $category = new category;
      $category->name = $request->name;
      $category->save();

      return response()->json(['msg' => 'categoty created successfully']);
    }

    //addsubCategory
    public function addsubCategory(Request $request)
      {

        if (!$request->name)
            return response()->json(['msg' => 'name_Required']);
        if (!$request->parent_id)
            return response()->json(['msg' => 'parent_id_Required']);

        $category = new category;
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->save();

        return response()->json(['msg' => 'subcategoty created successfully']);
      }

//All categories
 public function AllCategory()
  {
    $cat = category::all();
    return response()->json(['result' => $cat]);
  }
//subcategory
public function subcategory($id)
 {
     $subcats  = category::where('parent_id', $id)->get();
   return response()->json(['result' => $subcats]);
 }
}
