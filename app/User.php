<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Nicolaslopezj\Searchable\SearchableTrait;

class User extends Authenticatable
{
    use Notifiable;
    use SearchableTrait;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','tokens'
    ];

    protected $searchable = [
        'columns' => [
            'users.name' => 10,
            'users.email' => 1,
            'users.id' => 3,
            'users.phone' => 10,
        ]
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Ads()
      {
            return $this->hasMany('App\Ads');
      }

    public function comments()
      {
          return $this->hasMany('App\Comments');
      }

    public function follower()
    {
        return $this->belongsToMany(User::class, 'followers', 'user_id', 'follower_id');
    }

    public function following()
    {
        return $this->belongsToMany(User::class, 'followers', 'follower_id', 'user_id');
    }

    public function Message()
    {
        return $this->hasMany('App\Message','from_user');
    }
    public function like()
      {
          return $this->hasMany('App\Like');
      }
    public function fav()
    {
            return $this->hasMany('App\Favourite');
    }

}
