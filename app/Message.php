<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
  protected $table = 'messages';

  protected $fillable = [
    'from_user', 'to_user', 'content',
  ];

  public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
