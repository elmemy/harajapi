<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Ads extends Model
{
  protected $table = 'ads';

  use SearchableTrait;

  protected $fillable = [
    'title', 'code_product','image','phone','year','user_id'
  ];

  protected $searchable = [
        'columns' => [
            'ads.title' => 10,
            'ads.address' => 5,
            'ads.phone' => 3,
            'ads.parent_id' => 3,


        ]
    ];

  public function User()
   {
     return $this->belongsTo('App\User');
   }

  public function comment()
    {
           return $this->hasMany('App\Comments');
    }

  public function like()
    {
       return $this->hasMany('App\Like','Ads_id');
    }
  public function fav()
    {
     return $this->hasMany('App\Favourite');
    }


  public function cat()
     {
       return $this->belongsTo('App\category','parent_id');
     }

public function address()
    {
      return $this->belongsTo('App\Address');
     }

}
