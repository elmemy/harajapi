<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ads;
use App\User;

class Comments extends Model
{
  public function ads()
  {
      return $this->belongsTo(Ads::class, 'ads_id', 'id');
  }
  public function user()
  {
      return $this->belongsTo(User::class, 'user_id', 'id');
  }
}
